

#third part
#task1
import gzip
import json

def read_categories(filepath):
    with gzip.open(filepath, 'rt', encoding='utf-8') as file:
        return json.load(file) #load will convert json data into python obj 

# Example usage:
categories_data = read_categories("third_part/categories.json.gz")
print(json.dumps(categories_data, indent=4)) #dump vice versa of load

#task2
scrapy startproject woolworths
import scrapy

class WoolworthsSpider(scrapy.Spider):
    name = 'woolworths'
    start_urls = ['https://www.woolworths.com.au/shop/browse/drinks/cordials-juices-iced-teas/iced-teas']

    def parse(self, response):
        # Extract breadcrumb using XPath
        breadcrumb = response.xpath('//nav[@class="breadcrumb"]/ul/li/a/text()').getall()

        # Extract product names using XPath
        product_names = response.xpath('//div[@class="title"]/text()').getall()
        
        for product_name in product_names:
            yield {
                'product_name': product_name.strip(),
                'breadcrumb': breadcrumb
            }
scrapy crawl woolworths -o output.csv


#task3
import scrapy

class EdekaSpider(scrapy.Spider):
    name = 'edeka'
    start_urls = ['https://www.edeka24.de/Lebensmittel/Suess-Salzig/Schokoriegel/']

    def parse(self, response):
        # Extracting product names and prices
        products = response.xpath('//div[contains(@class, "item ")]')
        
        for product in products:
            name = product.xpath('.//a[@class="link-default"]//text()').get().strip()
            price = product.xpath('.//div[@class="price"]//text()').get().strip()
            
            yield {
                'product_name': name,
                'product_price': price
            }

