#task1
import requests

def http_request():
    url = 'https://eoa0qzkprh8wd6s.m.pipedream.net'
    response = requests.get(url)
    
    if response.status_code == 200:
        data = response.json()
        if 'success' in data:
            return data['success']
        else:
            return "Unexpected response format"
    else:
        return "Request failed: {}".format(response.status_code)

print(http_request())

#task2
curl -H "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36" -H "Accept: application/json" https://eo6f3hp6twlkn24.m.pipedream.net/

# Your code here

# task 4
class CacheDecorator:
    """Saves the results of a function according to its parameters"""
    def __init__(self):
        self.cache = {}

    def __call__(self, func):
        def _wrap(*a, **kw):
            if a[0] not in self.cache:
                self.cache[a[0]] = func(*a, **kw)
            return self.cache[a[0]]

        return _wrap

# task 5

class LoginMetaClass(type):
    pass


class AccessWebsite(metaclass=LoginMetaClass):
    logged_in = False

    def login(self, username, password):
        if username == "admin" and password == "admin":
            self.logged_in = True

    def access_website(self):
        return "Success"
